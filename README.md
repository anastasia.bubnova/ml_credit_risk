# ml_credit_risk

Процесс загрузки файлов с kaggle:
```angular2html
!kaggle --version ---если kaggle не установлен --> скачиваем командой ниже
!pip install kaggle --upgrade

cd /Users/mac/ml_credit_risk/data/raw ---перемещаемся в нужную нам папку
ls --проверяем, что мы действительно в ней и какие в ней есть файлы

!cp ~/Downloads/kaggle.json ~/.kaggle/kaggle.json
!chmod 600 ~/.kaggle/kaggle.json

---скачиваем данные---
!kaggle competitions download -c home-credit-default-risk -p ./data/raw
!unzip './data/raw/home-credit-default-risk.zip' -d './data/raw'
!rm './data/raw/home-credit-default-risk.zip'
```
Дополнительные команды:
```angular2html

!kaggle competitions files home-credit-default-risk --выводим список всех файлов соревнования
```
